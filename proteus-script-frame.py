#!/usr/bin/env python3

# General usable frame for proteus scripts. It handles the initial
# steps to get connected to Tryton with the help of command line
# optons. You can extend it with your own code.

# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse, configparser, os, sys, time
import logging
import proteus

def parse_arguments(logger):
    """Parse and check program arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", metavar="FILE",
                        help="tryton config file (if not used, TRYTOND_CONFIG environment must be set)")
    parser.add_argument("-d", "--database", required=True,
                        help="name of database")
    parser.add_argument("-v", "--verbose", action="count", default=0,
                        help="enable verbose mode (-vv for more details)")
    parser.add_argument("-n", "--dry-run", action="store_true", help="don't save changes")
    # Add here your own options and parameters

    args = parser.parse_args()

    # Check if config option exists; if not, check for
    # environment TRYTOND_CONFIG; else raise error
    if args.config:
        config_file = args.config
        if args.verbose > 1:
            logger.debug("config option used")
    elif os.environ.get("TRYTOND_CONFIG"):
        config_file = os.environ.get("TRYTOND_CONFIG")
        if args.verbose > 1:
            logger.debug("environment TRYTOND_CONFIG used")
    else:
        logger.error("wether config option nor TRYTOND_CONFIG environment is available")
        raise SystemExit("script stopped")

    config = configparser.ConfigParser()
    try:
        with open(config_file, "r") as fh:
            config.read_file(fh)
    except FileNotFoundError as error:
        logging.error(str(error))
        raise SystemExit("script stopped")

    # Check for existing 'database' section an 'uri' entry in the config file
    if 'database' not in config:
        logger.error("missing [database] section in config file")
        raise SystemExit("script stopped")
    if 'uri' not in config['database']:
        logger.error("missing uri entry in [database] section")
        raise SystemExit("script stopped")

    if args.verbose > 1:
        logger.debug("database: %s", args.database)
        logger.debug("dry-run: %s", args.dry_run)
        logger.debug("config file: %s", config_file)
        # Add here your own options and parameters

    return (args, config_file)


def do_it(verbose, logger):
    """Here is some sample code. Replace it with your own code."""
    Party = proteus.Model.get('party.party')
    party, = Party.find(['id', '=', '1'])
    if verbose > 0:
        logger.info(party.name)


def main():
    """Main function"""

    # Set timezone to UTC to avoid different timezones in the logger
    # timestamp (because trytond uses UTC as timezone instead of the local one).
    os.environ['TZ'] = "UTC"
    time.tzset()

    # To reduce possible tryond log debug messages on stdout
    # the logging level is set to ERROR.
    # (see https://discuss.tryton.org/t/deactivating-trytond-logging-when-running-a-proteus-script/6646/5)
    for logger in ['trytond.modules', 'trytond.pool', 'trytond.cache',
                   'trytond.backend.postgresql.database', 'trytond.backend.sqlite.database']:
        log = logging.getLogger(logger)
        log.setLevel(logging.ERROR)

    # Config the logger for the script
    # instead of stream=sys.stdout you can use filename='/path/to/your/logfile.log'
    logging.basicConfig(stream=sys.stderr,
                        format="[{asctime}] {levelname}:{name}:{message}",
                        style="{",
                        datefmt="%Y-%m-%d %H:%M:%S %Z",
                        )
    # Change the name to your own script name
    logger = logging.getLogger('proteus-script-frame')
    logger.setLevel(logging.DEBUG)

    # Parse command line arguments
    (args, config_file) = parse_arguments(logger)
    
    # Connect to trytond
    try:
        confProteus = proteus.config.set_trytond(database=args.database, config_file=config_file)
    except OSError as error:
        # Can't find the database
        logging.error(str(error))
        raise SystemExit("script stopped")
    
    # Execute your code
    do_it(args.verbose, logger)


if __name__ == "__main__":
    main()
